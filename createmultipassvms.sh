#!/bin/sh

VMS=1
MEM=1G
DISK=15G
CPUS=1

create_vm () {
    # Create images
    multipass launch -d $DISK -m $MEM -c $CPUS -n python$i

    # update apt
    multipass exec python$i -- sudo apt-get -qq update

    # install ansible
    multipass exec python$i -- sudo apt-get install -qq -y ansible

    # download the git repo
    multipass exec python$i -- git clone https://gitlab.com/scottsyms/ansiblescripts.git

    # add localhost to respective /etc/ansible/hosts files
    multipass exec python$i -- sudo ansiblescripts/addhost.sh

    # Run playbook for each environment
    multipass exec python$i -- ansible-playbook -b ansiblescripts/ipython.yml

    multipass exec python$i -- mkdir /home/ubuntu/data

    multipass exec python$i -- ansiblescripts/build.sh

    echo "Created vm python$i"
    }

for i in $(seq $VMS)
do
    create_vm
done

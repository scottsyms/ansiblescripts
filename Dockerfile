# Start from a core stack version
FROM jupyter/all-spark-notebook:latest

# Install the Dask dashboard
RUN pip install --quiet --no-cache-dir dask-labextension jupyterlab-git && \
    fix-permissions "${CONDA_DIR}" && \
    fix-permissions "/home/${NB_USER}"

# Dask Scheduler & Bokeh ports
EXPOSE 8787
EXPOSE 8786

ENTRYPOINT ["jupyter", "lab", "--ip=0.0.0.0", "--allow-root", "-NotebookApp.password='argon2:$argon2id$v=19$m=10240,t=10,p=8$9OCmMthsCe4KQAV+Ifp7ow$a6+wXrIve1pFvDKyQtEE2A'"]
